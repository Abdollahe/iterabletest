

public class Main {

    public static void main(String[] args) {

        String str1 = "This is test for iterable number 1" ;
        String str2 = "Testing iterable 2 with different string " ;

        StringIterable stringIterable = new StringIterable(str1 , str2) ;


        Iterable<Character> iterable1 = stringIterable.getIterable1() ;
        Iterable<Character> iterable2 = stringIterable.getIterable2() ;


        System.out.println("--------------------------------------------------");
        System.out.println("Iterating through the first iterable results in: ");
        iterable1.forEach(System.out::print);

        System.out.println("\n--------------------------------------------------");

        System.out.println("Iterating through the second iterable results in: ");
        iterable2.forEach(System.out::print);
        System.out.println("\n--------------------------------------------------");


    }
}
