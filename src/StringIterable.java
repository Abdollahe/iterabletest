import java.util.Iterator;


class StringIterable {

    private Iterable<Character> iterable1 ;
    private Iterable<Character> iterable2 ;


    /**
     * Constructor -> Constructor for creating iterables from String objects.
     * This class creates two distinct iterables from two String objects but
     * can be generalized to more number of String objects
     * @param str1 -> First String object to create an iterable from
     * @param str2 -> Second String object to create an iterable from
     * **/
    StringIterable(String str1 , String str2) {

        char[] charArray1 = str1.toCharArray() ;
        char[] charArray2 = str2.toCharArray() ;

        iterable1 = () -> new Iterator<>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return charArray1.length > index;
            }

            @Override
            public Character next() {
                return charArray1[index++];
            }
        };

        iterable2 = () -> new Iterator<>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return charArray2.length > index;
            }

            @Override
            public Character next() {
                return charArray2[index++];
            }
        };

    } // end of constructor


    /**
     * Getters for the iterables.
     * **/
    Iterable<Character> getIterable1() {
        return iterable1;
    }

    Iterable<Character> getIterable2() {
        return iterable2;
    }
}
